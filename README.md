# JoinPeerTube

## Install dependencies

Fetch the dependencies

```
$ npm ci
```

## Dev

```
$ npm run dev
```

Will run a dev server that will render files on the fly.

## Build for production

```
$ npm run build
```

Routes are pre-rendered in `dist`. You need to use `node server/prod-server.js ./dist` to correctly serve these files.

## Update translations

Add Weblate remote:

```
$ git remote add weblate https://weblate.framasoft.org/git/joinpeertube/main
```

Update from Weblate:

```
$ git fetch weblate && git merge weblate/master
```

Re generate translations:

```
$ npm run i18n:update
```

Push on master (Weblate will be automatically updated)

```
$ git push origin master
```

## News

To add a news, add markdown files in `src/news/en` and `src/news/fr` and rebuild `npm run build`.
To archive a news, move it in `src/news/archives` and rebuild `npm run build` (you can also use `npm run build:news-meta` and `npm run dev`).


## Add locale

Add the locale in `src/shared/i18n.js` and `gettext.config.js`. Then update translations.

(`prod-server.js` needs to be restarted after an update on production)
