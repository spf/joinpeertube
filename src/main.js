import { createSSRApp, createApp as createClientApp } from 'vue'
import { createGettext } from 'vue3-gettext'
import { createHead } from '@vueuse/head'
import VueProgressBar from '@aacassandra/vue3-progressbar'

import App from './App.vue'

import './scss/main.scss'

import SafeHTML from './components/SafeHTML.vue'
import Noop from './components/Noop.vue'
import CommonMixins from './mixins/CommonMixins'
import { getDefaultLocale, getAllLocales, getAvailableLocales, isDefaultLocale, getCompleteLocale } from './shared/i18n'

import { buildRouter } from './router'

async function createApp (ssrLocale) {
  const app = isInClientMode()
    ? createClientApp(App)
    : createSSRApp(App)

  cleanupOldLocalStorage()

  const { currentLanguage, promise } = ssrLocale
    ? buildSSRLocalePromise(app, ssrLocale)
    : buildTranslationPromiseForCurrentLocale(app)

  const translations = await promise

  const gettext = createGettext({
    translations,
    availableLanguages: getAvailableLocales(),
    defaultLanguage: currentLanguage,
    mutedLanguages: [ 'en_US' ],
    silent: true
  })

  app.use(gettext)

  // ###########################

  app.mixin(CommonMixins)
  app.component('SafeHTML', SafeHTML)

  const router = await buildRouter()
  app.use(router)

  await useMatomo(app, router)

  const head = createHead()
  app.use(head)

  if (import.meta.env.SSR) {
    // eslint-disable-next-line vue/component-definition-name-casing
    app.component('vue-progress-bar', Noop)
  } else {
    app.use(VueProgressBar, { color: '#F2690D' })
  }

  return { app, router, head }
}

export {
  createApp
}

// ---------------------------------------------------------------------------

async function useMatomo (app, router) {
   if (import.meta.env.SSR) return

   if (navigator.doNotTrack === 'yes' || navigator.doNotTrack === '1') return

  const VueMatomo = await import('vue-matomo')

  app.use(VueMatomo.default, {
    // Configure your matomo server and site
    host: 'https://stats.framasoft.org/',
    siteId: 68,

    // Enables automatically registering pageviews on the router
    router,

    // Require consent before sending tracking information to matomo
    // Default: false
    requireConsent: false,

    // Whether to track the initial page view
    // Default: true
    trackInitialView: true,

    // Changes the default .js and .php endpoint's filename
    // Default: 'piwik'
    trackerFileName: 'p',

    enableLinkTracking: true
  })
}

// ---------------------------------------------------------------------------

function cleanupOldLocalStorage () {
  if (!hasLocalStorage()) return

  const isOldTranslationKey = key => {
    for (const start of [ 'translations-v1-', 'translations-v2-', 'translations-v3-', 'translations-v4-' ]) {
      if (key.startsWith(start)) return true
    }

    return false
  }

  try {
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i)

      if (isOldTranslationKey(key)) localStorage.removeItem(key)
    }
  } catch (err) {
    console.error('Cannot remove old translations keys')
  }
}

// ---------------------------------------------------------------------------

async function buildTranslationsPromise (currentLanguage) {
  // No need to translate anything
  if (isDefaultLocale(currentLanguage)) return Promise.resolve({})

  // Fetch translations from server
  const translationModule = await import(`./translations/${currentLanguage}.json`)

  return translationModule.default
}

function buildSSRLocalePromise (app, ssrLocale) {
  const locale = getCompleteLocale(ssrLocale).replace('-', '_')

  app.config.globalProperties.localePath = locale

  return {
    currentLanguage: locale,
    promise: buildTranslationsPromise(locale)
  }
}

function buildTranslationPromiseForCurrentLocale (app) {
  let currentLanguage = getDefaultLocale()

  const navigatorLocale = window.navigator.userLanguage || window.navigator.language
  const snakeCaseLocale = navigatorLocale.replace('-', '_')
  const completeLocale = getCompleteLocale(snakeCaseLocale)

  // Supported locale
  if (getAllLocales()[completeLocale]) {
    currentLanguage = completeLocale
  }

  app.config.globalProperties.localePath = currentLanguage

  return {
    currentLanguage,
    promise: buildTranslationsPromise(currentLanguage)
  }
}

// ---------------------------------------------------------------------------

function hasLocalStorage () {
  try {
    return typeof localStorage !== 'undefined'
  } catch {
    return false
  }
}

// ---------------------------------------------------------------------------

function isInClientMode () {
  if (import.meta.env.SSR) return false
  if (document.documentElement.lang) return false

  return true
}
