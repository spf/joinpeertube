function getAllLocales () {
  return Object.keys(getAvailableLocales()).concat(Object.keys(getAliasedLocales()))
}

function getDirection (locale) {
  return new Set([ 'ar', 'fa' ]).has(locale)
    ? 'rtl'
    : 'ltr'
}

function getAvailableLocales () {
  return {
    'ar': 'العربية',
    'cs': 'Čeština',
    'cy': 'Cymraeg',
    'de': 'Deutsch',
    'en_US': 'English',
    'eo': 'Esperanto',
    'es': 'Español',
    'fr_FR': 'Français',
    'gd': 'Gàidhlig',
    'gl': 'galego',
    'hr': 'hrvatski',
    'hu': 'magyar',
    'id': 'Indonesia',
    'is': 'Íslenska',
    'it': 'Italiano',
    'ja': '日本語',
    'pl': 'Polski',
    'pt_BR': 'Português',
    'ru': 'русский',
    'sq': 'Shqip',
    'sv': 'svenska',
    'uk': 'украї́нська мо́ва',
    'sr_Cyrl': 'Српска ћирилица',
    'th': 'ไทย',
    'tr': 'Türkçe',
    'zh_Hans': '简体中文（中国）',
    'zh_Hant': '繁體中文（台灣）'
  }
}

function getAliasedLocales () {
  return {
    'en': 'en_US',
    'fr': 'fr_FR',
    'pt': 'pt_BR'
  }
}

function isDefaultLocale (locale) {
  return locale === 'en_US' || locale === 'en'
}

function getDefaultLocale () {
  return 'en_US'
}

function getCompleteLocale (locale) {
  const alias = getAliasedLocales()

  return alias[locale] ?? locale
}

function getShortLocale (locale) {
  return locale.toLowerCase().split('_')[0]
}

export {
  getAllLocales,
  getDirection,
  getAvailableLocales,
  isDefaultLocale,
  getAliasedLocales,
  getDefaultLocale,
  getCompleteLocale,
  getShortLocale
}
