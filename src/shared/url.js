import { getAllLocales } from './i18n.js'

function getLocaleFromPath (path) {
  const matched = path.match(new RegExp('^/([^/]+)'))
  if (!matched) return undefined

  const maybeLocale = matched[1]
  if (getAllLocales().includes(maybeLocale) !== true) return undefined

  return maybeLocale
}

function isExcludedPath (path, exclude404) {
  if (exclude404 === true && path.endsWith('/404')) return true
  if (path.includes(':pathMatch')) return true

  return false
}

export {
  getLocaleFromPath,
  isExcludedPath
}
