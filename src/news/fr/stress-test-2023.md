---
id: stress-test-2023
title: "Tests de stress PeerTube : la résilience réside dans les pairs !"
date: December 21, 2023
---

De nombreux administrateurs ou vidéastes se demandent combien de spectateurs simultanés une instance PeerTube peut gérer et dans quelle mesure la fonctionnalité P2P peut aider le serveur à diffuser une vidéo en direct.

C'est la raison pour laquelle, avec le soutien de [NLnet](https://nlnet.nl/) et [Octopuce](https://www.octopuce.fr/), nous avons effectué des tests de charge *en conditions réelles* sur une instance PeerTube classique. Bien sûr dans le but d'optimiser le logiciel, mais avant tout pour pouvoir donner avec confiance le nombre de spectateurs simultanés qu'une instance PeerTube devrait être capable de gérer et comprendre comment le P2P se comporte dans de telles conditions.

Nous avons choisi de simuler 1 000 spectateurs car il s'agit d'un nombre symbolique, mais aussi parce [qu'il couvre 99 % des flux diffusés sur Twitch en 2022](https://twitter.com/zachbussey/status/1543309398247874566). PeerTube serait donc capable de gérer techniquement 99% de ces cas d'utilisation, ce qui représente un grand nombre d'utilisateurs ;)

Si vous préférez sauter les détails techniques, vous pouvez passer directement à la <a href="#stress-test-2023-conclusion">conclusion</a>.

#### Mise en place du benchmark

Pour nous rapprocher des conditions réelles, nous avons décidé de faire tourner 1 000 navigateurs web Chrome regardant la même vidéo, chacun ayant une IP publique dédiée pour simuler correctement de vrais spectateurs.

Afin d'exécuter 1 000 navigateurs web, nous avons décidé de créer une [grille Selenium](https://www.selenium.dev/documentation/grid/) contenant 1 000 nœuds à l'aide de Docker. De cette manière, chaque nœud peut avoir sa propre adresse publique IPv6. Luc, l'excellent administrateur système de Framasoft, a développé des scripts pour générer automatiquement cette grille Selenium en utilisant le [cloud d'Hetzner](https://www.hetzner.com/). Son travail est disponible sur https://framagit.org/framasoft/peertube/selenium-stack.

Après les premiers tests concluants où nous avons atteint 500 navigateurs web, nous avons rencontré des difficultés avec le cloud d'Hetzner. En effet, ils nous ont refusé l'augmentation de nos quota VPS nous empêchant d'atteindre 1 000 navigateurs web. Après avoir passé plusieurs jours à chercher des alternatives, [Octopuce](https://www.octopuce.fr/), un hébergeur français qui gère plusieurs instances PeerTube, nous a proposé d'utiliser un de leur serveur pour nous aider à atteindre notre objectif de 1 000 navigateurs web. Un grand merci à eux !

Nous avons réalisé plusieurs améliorations de performance dans le noyau de PeerTube afin d'atteindre les 1 000 spectateurs. Certaines d'entre elles sont déjà disponibles dans [PeerTube V6](https://joinpeertube.org/news/release-6.0) comme l'optimisation des événements des *vues*. D'autres seront disponibles dans la prochaine version (V6.1) : possibilité de personnaliser l'intervalle entre les événements des *vues*, nouveau protocole de fédération des *vues* permettant d'envoyer beaucoup moins de messages, etc.

Une fois que la grille Selenium est prête et que l'instance PeerTube est mise à jour pour inclure les améliorations de performance ci-dessus, nous pouvons lancer 1 000 navigateurs web qui chargent une vidéo sur https://peertube2.cpy.re/ (notre instance PeerTube mise à jour chaque nuit) en utilisant [WebdriverIO](https://webdriver.io/). Chaque navigateur automatisé est programmé pour charger la [page web](https://peertube2.cpy.re/w/1zywKcr1ChzL7R9rG6yCnq), lire la vidéo et attendre la fin du test.


#### Conditions du benchmark

L'instance PeerTube de test a été installée en suivant le [guide d'installation officiel](https://docs.joinpeertube.org/install/any-os) sur Debian 12.2 avec nginx, PostgreSQL et Redis sur la même machine.

Voici les spécifications matérielles :
  * 4 vCore de i7-8700 CPU @ 3.20GHz
  * Dispose d'un disque dur (pas de SSD)
  * 4Go de RAM
  * Réseau 1Gbit/s

Et les [paramètres de configuration](https://docs.joinpeertube.org/maintain/configuration#scalability) importants de l'instance PeerTube :
  * Le plugin de chat n'est pas activé
  * Les logs sont en mode *warning*
  * Les logs clients sont activés
  * Les métriques sont activées mais la métrique de la durée des requêtes HTTP est désactivée
  * La fédération des spectateurs V2 est activée (fonctionnalité derrière un *feature flag* que nous prévoyons d'activer dans PeerTube 6.2)
  * Le stockage d'objets (S3) n'est pas activé

Les vidéos/lives testées sont publiques, les fichiers statiques sont donc directement servis par nginx.

La vitesse du réseau du navigateur web est limitée par l'utilisation de :

```ts
browser.setNetworkConditions({
  offline: false,
  download_throughput: 2000 / 8 * 1024, // 2000kbit/s,
  upload_throughput: 300 / 8 * 1024, // 300kbit/s
  latency: 500
})
```

Malheureusement, les *network conditions* ne s'appliquent pas à WebRTC donc nous n'avons pas pu limiter la bande passante en P2P. Ces paramètres ne s'appliquent qu'aux requêtes HTTP.

#### Résultats du benchmark

Nous avons effectué des tests de stress via 4 scénarios :
  * Une vidéo en direct avec un paramètre de latence *normal*
  * Une vidéo en direct avec une latence *élevée*
  * Une vidéo en direct avec une latence *élevée* où la moitié des spectateurs ont désactivé le P2P
  * Une vidéo VOD normale

Les vidéos en direct n'offrent qu'une seule définition avec un débit de 650 kbit/s, tandis que les vidéos VOD ont 4 définitions, la plus élevée ayant un débit de 1,2 Mbit/s.

Voici les résultats de nos 4 scénarios dans lesquels 1 000 spectateurs se arrivent sur la vidéo/le live en quelques minutes.

##### Live avec une latence normale

![graphique de latence normale pour une vidéo en directe](/img/news/stress-test-2023/live-normal-latency.png)

En utilisant les paramètres par défaut et une latence d'environ 30 secondes, nous pouvons voir que l'utilisation du CPU de PeerTube atteint un pic lorsque les navigateurs web chargent la vidéo (`11:08:00`) et diminue lorsque les spectateurs regardent la vidéo (`11:11:00`). La tâche principale de PeerTube à ce moment-là est de gérer les métriques de lecture et les événements des *vues* provenant des navigateurs web pour les statistiques et la fédération. La consommation de RAM reste stable.

La plupart des spectateurs téléchargent la vidéo en utilisant le protocole HTTP lorsqu'ils chargent la page pour mettre en mémoire tampon les segments, puis essaient progressivement de télécharger des segments plus éloignés en utilisant le P2P. C'est la raison pour laquelle nous voyons un pic de téléchargement HTTP de 150Mbit/s au début du graphique (`11:08:00`) qui chute progressivement à 90Mbit/s (`11:12:00`). À ce stade, les navigateurs web échangent principalement des segments en utilisant le P2P à un débit pouvant atteindre 370 Mbit/s. Dans des conditions optimales, l'aspect P2P de PeerTube réduit la bande passante nécessaire à la diffusion d'une vidéo en direct d'un facteur 3 à 4, ce qui corrobore les commentaires reçus de certains administrateurs de PeerTube.

##### Live avec une latence élevée

Nous avons voulu tester un live avec le paramètre *Latence importante* (~60 secondes) pour que les navigateurs web aient plus de temps pour télécharger les segments.

![graphique de latence élevée pour une vidéo en directe](/img/news/stress-test-2023/live-big-latency.png)

Nous avons maintenant un ratio de 65Mbit/s pour HTTP et 370Mbit/s pour P2P (`15:25:00`). C'est une belle amélioration, mais nous pensons pouvoir améliorer le ratio HTTP/P2P à l'avenir en modifiant certains paramètres de notre moteur P2P.

##### Live avec une latence élevée et la moitié des spectateurs ont le P2P désactivé

Ce scénario tente d'imiter les spectateurs du "monde réel" en désactivant le P2P pour la moitié d'entre eux.

![graphique de latence élevée pour une vidéo en directe avec la moitié des spectateurs dont le P2P est désactivé](/img/news/stress-test-2023/live-big-latency-half-p2p-disabled.png)

Nous avons un ratio de 260Mbit/s pour HTTP et 190Mbit/s pour P2P (`15:42:00`). Les 500 spectateurs avec le P2P activé échangent les segments avec le même ratio que le direct avec une latence élevée, tandis que les spectateurs dont le P2P est désactivé se contentent de télécharger les segments à partir du serveur.

##### Vidéo VOD

Il est intéressant de se concentrer sur les vidéos en direct et d'analyser leur ratio P2P puisque les spectateurs regardent et partagent simultanément les mêmes segments. Mais nous pouvons également imaginer des cas d'utilisation lorsqu'une vidéo VOD devient virale :

![graphe pour une vidéo VOD](/img/news/stress-test-2023/vod.png)

La consommation de bande passante est beaucoup plus élevée que pour un direct parce que le débit vidéo est plus élevé, mais aussi parce que le navigateur web met beaucoup plus de vidéo en mémoire tampon, surtout si des segments sont disponibles en P2P. C'est la raison pour laquelle nous avons un pic de 2 000 Mbit/s pour le P2P au début, chutant progressivement à 1 150 Mbit/s (`15:10`).

Si nous zoomons sur le graphique HTTP :

![zoom dans le graphe HTTP de la vidéo VOD](/img/news/stress-test-2023/vod-http-zoom.png)

Nous observons que les navigateurs web téléchargent les segments proches en HTTP pour éviter les problèmes de lecture, puis essaient de télécharger les segments éloignés en P2P. C'est pourquoi nous avons un pic de 200Mbit/s pour le téléchargement HTTP au début du graphique (`15:09:00`).

Après quelques minutes, le rapport P2P/HTTP devient très important avec 1150Mbit/s pour P2P et 25Mbit/s pour HTTP (`15:14:00`). Cela signifie que le P2P fonctionne très bien sur les vidéos VOD lorsque les spectateurs regardent les mêmes parties de la vidéo. C'est un comportement attendu puisque nous avons plus de temps pour échanger et mettre en mémoire tampon les segments éloignés en utilisant le P2P.

En bonus, voici l'écran de mon navigateur web personnel pendant le test où l'on peut voir que dans une situation optimale avec connection en fibre optique, les spectateurs "classiques" peuvent avoir un très grand ratio P2P (800MB téléversés/100MB téléchargés).

![image où l'on peut voir que le navigateur web a téléchargé 100MB et envoyé 806MB de la vidéo](/img/news/stress-test-2023/vod-example-p2p-player.png)

#### Aperçu technique

Voici un aperçu du ratio P2P de la vidéo en direct avec une latence normale et de la vidéo VOD avec 1 000 spectateurs :

<table class="table">
  <thead>
    <tr>
      <th scope="column"></th>
      <th scope="column">HTTP en pic</th>
      <th scope="column">HTTP après 5 minutes</th>
      <th scope="column">P2P après 5 minutes</th>
      <th scope="column">Ratio HTTP/P2P après 5 minutes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Live</th>
      <td>150 Mbit/s</td>
      <td>90 Mbit/s</td>
      <td>350 Mbit/s</td>
      <td>25% (le P2P économise 75% de la bande passante)</td>
    </tr>
    <tr>
      <th scope="row">VOD</th>
      <td>200 Mbit/s</td>
      <td>25 Mbit/s</td>
      <td>1150 Mbit/s</td>
      <td>2% (le P2P économise 98% de la bande passante)</td>
    </tr>
  </tbody>
</table>

Nous considérons que ces valeurs sont valables dans des conditions optimales, puisque nos navigateurs web simulés disposaient d'une connexion internet rapide pour le P2P que nous ne pouvions pas limiter. Mais d'après notre expérience, elles semblent être proches ce qui se passe dans la vie réelle.

<span id="stress-test-2023-conclusion"></span>

#### Conclusion et futurs travaux

Avec ces résultats, nous pouvons voir qu'un site web PeerTube ordinaire (location de serveur à environ 20€/mois) peut correctement gérer 1 000 spectateurs simultanés si l'administrateur suit notre [guide de configuration](https://docs.joinpeertube.org/maintain/configuration#scalability).

Cela signifie que PeerTube, un logiciel libre financé par des dons et des subventions des programmes NGI, qui a été développé pendant 6 ans par des contributeurs bienveillants et un développeur rémunéré, offre une alternative abordable, résiliente, efficace et solide à la technologie des géants du numérique. C'est peut-être difficile à réaliser, mais c'est vrai : ensemble, on l'a fait.

Même si gérer 1 000 spectateurs simultanés est une belle réussite, PeerTube peut encore aller plus loin [avec une configuration plus poussée](https://docs.joinpeertube.org/maintain/configuration#scalability).

Nous avons également quelques idées pour améliorer la gestion d'encore plus de spectateurs simultanés dans l'avenir :
  * Optimiser les paramètres de notre moteur P2P lorsque la *latence élevée* est définie pour les vidéos en direct
  * Pouvoir spécifier des trackers P2P externes dans la configuration de PeerTube, qui seraient plus adaptés à la gestion d'un plus grand nombre de pairs
  * Distribuer le travail pour gérer les événements et statistiques de lecture des navigateurs sur plusieurs machines
  * Charger dynamiquement certains composants dans le client (comme la section des commentaires) pour éviter de faire des requêtes HTTP si les composants ne sont pas encore visibles dans le navigateur web

Merci pour votre lecture et n'hésitez pas à partager vos expériences avec PeerTube et/ou les limites que vous avez rencontrées, nous serons heureux de travailler dessus. N'oubliez pas non plus de soutenir notre travail si vous le pouvez, et de partager les bonnes nouvelles !

<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" rel="nofollow noreferrer noopener" href="https://support.joinpeertube.org/">
      Soutenir Framasoft
   </a>
</div>
