---
id: release-6.1
title: "La 6.1 de PeerTube est sortie !"
date: April 29, 2024
---

Nous avons le plaisir de vous annoncer la sortie de la version 6.1 de PeerTube 🙂

Outre l'intégration de l'import/export de compte et autres belles fonctionnalités que nous allons détailler dans cet article de blog, cette version corrige 2 failles de sécurité importantes concernant la fédération ActivityPub (détaillées en fin d'article).

Nous vous recommandons donc de mettre à jour vos instances rapidement !

#### Exportation et importation de compte

Il est désormais possible d'exporter toutes les données de son compte (vidéos, chaînes, préférences...) dans une archive librement téléchargeable. Cette archive peut-être utilisée comme outil de sauvegarde, mais aussi être ré-importée sur une instance PeerTube différente. Précisons tout de suite : ce **n'est pas** (encore) une migration de compte ! Les données (comme les chaînes ou les vidéos) sont **dupliquées** et **non déplacées** depuis votre ancien compte PeerTube sur votre nouvelle instance.

Vous trouverez la liste des données qui seront, ou ne seront pas, exportées dans [notre documentation](https://docs.joinpeertube.org/fr/use/setup-account#exportation-du-compte) !

![capture d'écran de l'export](/img/news/release-6.1/fr/FR_export_compte.png)

Cette fonctionnalité permet de réduire la difficulté à créer son compte sur "la bonne" instance PeerTube, car il est maintenant beaucoup plus facile d'en changer !

#### Fichier vidéo original conservé

Jusqu'à présent, quand vous téléversiez une vidéo dans PeerTube, celle-ci était transcodée en plusieurs formats et le fichier original était, quant à lui, supprimé. A partir de la v6.1 cette version sera conservée et pourra être téléchargée depuis votre bibliothèque et sera présent dans l'archive d'exportation !

Concrètement, cela peut vous permettre d'avoir **un archivage de vos vidéos sur une instance PeerTube** (plutôt que de prendre de la place à la fois sur votre disque dur personnel **et** sur une instance PeerTube en qualité inférieure).

![Capture d'écran de la vue téléchargement d'une vidéo](/img/news/release-6.1/fr/FR_video-download.png)

#### Bannière et avatar pour les instances

Pour personnaliser un peu plus votre instance, il est désormais possible d'ajouter une bannière qui sera visible dans la recherche d'instances sur [JoinPeerTube](https://joinpeertube.org/instances) mais aussi sur l'instance PeerTube sur les pages « À propos », de connexion, d'inscription, mais aussi sur la page d'accueil en passant par [la balise spéciale `<peertube-instance-banner>`](https://docs.joinpeertube.org/api/custom-client-markup#peertube-instance-banner) !

![Capture écran d'une vue de sepia search avec bannière](/img/news/release-6.1/fr/banniere_PeerTube_sepia_search.png)

L'avatar sera, quant à lui, visible sur [la future application mobile](https://joinpeertube.org/news/peertube-future-2024) et peut aussi [être utilisée](https://docs.joinpeertube.org/api/custom-client-markup#peertube-instance-avatar) la page d'accueil de votre instance !


#### La vue qui baisse

Afin de se rapprocher du comportement des autres plateformes de vidéos centralisatrices (Vimeo, Instagram, TikTok, Mux...), nous avons décidé de compter une *Vue* au bout de **10 secondes de visionnage** (plutôt que 30 secondes actuellement).

Mais ce n'est pas le seul critère qui change : une vue ne se base plus sur l'adresse IP de l'utilisateur mais sur un identifiant unique généré par le navigateur. Cela évite, par exemple, qu'une vidéo regardée par 20 personnes dans une université ne compte que pour une seule (car les postes ont généralement la même adresse IP).

On rappelle que des statistiques détaillées sur les vues et spectateurs sont aussi [accessibles pour le vidéaste](https://docs.joinpeertube.org/use/video-stats). PeerTube affiche d'ailleurs maintenant le nombre de spectateurs par *Région* en plus des *Pays*.

#### Sous titres plus accessibles

L'accès aux sous-titres a été facilité en ajoutant une icône directement dans le lecteur de vidéo. En cliquant sur le bouton **CC** (le 7 sur la capture d'écran), les sous-titres seront affichés dans la dernière langue utilisée (ou sinon la première disponible dans la liste). Vous pouvez aussi choisir une autre langue à partir des options (8 sur la capture d'écran).

![Capture d'écran du lecteur vidéo](/img/news/release-6.1/fr/video-player-watch.png)

#### ...et il y en a toujours plus !

Comme toujours, il y a les améliorations, des corrections de bugs et autres changements dont la liste exhaustive se trouve dans [le journal (en anglais)](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.1.0) mais dont voici une petite sélection :

  * Cette version corrige 2 failles de sécurité importantes sur la fédération ActivityPub. La première correction empêche la fuite de données liées à des vidéos privées (*j'aime*, commentaires etc.) et la deuxième corrige un contrôle d'accès incorrect des objets en provenance de la fédération. De plus amples détails seront communiqués sur cette deuxième faille de sécurité ultérieurement
  * Le turc rejoint la famille des 37 autres langues utilisables dans PeerTube ! Merci (*thank you*, *gracias*, *teşekkürler*) à toutes les personnes qui ont participé aux milliers de chaînes à traduire !
  * Côté UX (Expérience Utilisateurices) vous avez maintenant la possibilité de télécharger une vidéo directement depuis votre bibliothèque, et, pour les admins d'instances, la possibilité de filtrer par taille des fichiers stockés pour les vidéos et utilisateurs

Vous voulez nous aider à améliorer PeerTube ? Vous le pouvez **en partageant cette information**, en [proposant des idées d'améliorations](https://ideas.joinpeertube.org/) et, si vous pouvez vous le permettre, en faisant [un don à Framasoft](https://support.joinpeertube.org), l'association qui développe PeerTube.

Merci d'avance de votre soutien !

Framasoft
