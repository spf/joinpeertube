---
id: release-6.2
title: "La 6.2 de PeerTube est sortie !"
date: July 16, 2024
---
Cette nouvelle version est sous le signe de la simplification de la vie. Simplification de la modération, de la génération des sous-titres, de la mise en avant des vidéos. Regardons cela de plus près !

#### Modération des commentaires

La modération est toujours la partie la moins passionnante, et c'est aussi parfois la plus chronophage quand on publie des vidéos. Pour aider à faire un tri sélectif, la v6.2 de PeerTube ajoute non seulement une nouvelle page listant tous les commentaires faits sur vos vidéos, que vous pouvez maintenant approuver manuellement, mais aussi la possibilité de créer des listes de mots qui mettront automatiquement les commentaires les contenant en attente de modération !

Si vous souhaitez, par exemple, empêcher que des commentaires négatifs arrivent directement sur vos vidéos, vous pouvez créer une liste de mots :

![image autotag policies](/img/news/release-6.2/fr/moderation_autotag_policy.png)

Ainsi, les commentaires les contenant seront automatiquement mis en attente d'approbation :

![image d'une commentaire en attente de modération](/img/news/release-6.2/fr/moderation_comment_pending.png)

Libre à vous ensuite de les autoriser ou de les rejeter depuis la page listant les commentaires :

![image de la liste des commentaires côté vidéaste](/img/news/release-6.2/fr/moderation_comments_videos_list.png)

Cette fonctionnalité est aussi disponible pour les administrateurs ! Ainsi, il leur est possible d'afficher directement les vidéos téléversées sur leur instance qui contiennent certains mots clés. Cela peut s'avérer pratique pour surveiller des contenus contraires à votre charte ou des phénomènes sporadiques mais particulièrement viraux.

Cette idée de fonctionnalité découle notamment de l'étude qu'avait réalisé [l'ISD](https://joinpeertube.org/news/isd-study). Nous avons pour but de l'améliorer dans le futur où on imagine que les utilisateurs puissent s'abonner et se partager des listes de mots afin de faciliter la modération des vidéos !

#### Transcription automatique

Nous avons apporté dans la version 5.2 de PeerTube le concept de [*runner PeerTube*](https://docs.joinpeertube.org/admin/remote-runners), petit programme permettant de réaliser à distance les tâches lourdes de PeerTube (comme l'encodage des vidéos).

[France Université Numérique](https://www.fun-mooc.fr), un groupement d'intérêt public qui opère une plateforme de MOOC (Massive Open Online Course) avec beaucoup de contenus vidéos, a été intéressé par l'architecture flexible du runner PeerTube pour ses propres usages.

Dans une optique d'amélioration de l'accessibilité des vidéos tout en contribuant à l'écosystème PeerTube, France Université Numérique et la fondation [NLnet](https://nlnet.nl/project/PeerTube-mobile/) via le [fond NGI Zero Entrust](https://nlnet.nl/entrust/) ont financé le développement de la transcription automatique des vidéos au sein de PeerTube et des *runners PeerTube*. Cette transcription permet de créer automatiquement des sous-titres aux vidéos en utilisant un outil d'intelligence artificielle, [Whisper](https://fr.wikipedia.org/wiki/Whisper_(syst%C3%A8me_de_reconnaissance_vocale)), qui offre de très bon résultats. Le modèle d'intelligence artificiel utilisé est personnalisable dans la configuration.

Cette nouvelle fonctionnalité peut être activée par les administrateurs des plateformes PeerTube pour que les nouvelles vidéos publiées aient automatiquement des sous-titres générés. La création des sous-titres peut également être déclenchée à la demande, ce qui permettra aux administrateurs de créer les sous-titres des vidéos publiées avant la 6.2 de PeerTube. Cette transcription automatique peut aussi être réalisée par les *runners PeerTube*, car gourmande en puissance de calcul. C'est d’ores et déjà ce que compte utiliser France Université Numérique pour créer des sous-titres de l'ensemble de leur contenus, ce qui représente tout de même 3500 heures de vidéos.

Un grand merci à NLnet ([dont le financement risque malheureusement de se tarir](https://framablog.org/2024/07/14/lunion-europeenne-doit-poursuivre-le-financement-des-logiciels-libres/)) et France Université Numérique pour leur aide et contribution !

#### Et bien plus

PeerTube ne vous simplifie pas la vie qu'au niveau de la modération mais ajoute aussi une nouvelle façon de créer une vignette d'aperçu pour vos vidéos (en plus de la possibilité de la choisir depuis votre appareil) : directement depuis une image de votre vidéo ! Jouez la vidéo jusqu'à l'image illustrant parfaitement le contenu et affichez-la. Merci à [Kent Anderson](https://github.com/Chocobozzz/PeerTube/pull/6424) pour cette contribution externe !

Afin de ne pas passer à côté d'un direct en cours, ceux-ci sont maintenant mis en avant sur les pages *Ajoutées récemment*, *Tendances*, *Chaînes* et *Vidéos*.

Et comme à chaque nouvelle version, vous pouvez retrouver l'ensemble des corrections de bugs, améliorations et autres changements dans [le journal des modifications (en anglais)](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.0) !

---

Vous voulez nous aider à continuer d'améliorer PeerTube ? Vous le pouvez en partageant cette information, en [proposant des idées d'améliorations](https://ideas.joinpeertube.org/) (en anglais) et, si vous pouvez vous le permettre, en [faisant un don à Framasoft](https://support.joinpeertube.org/), l'association qui développe PeerTube.

Merci d'avance de votre soutien !
Framasoft
