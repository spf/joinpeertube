---
id: release-6.0
title: PeerTube v6 est publié, et conçu grâce à vos idées !
date: November 28, 2023
---

C'est #givingtuesday (« jour des dons »), donc nous vous offrons PeerTube v6 aujourd'hui ! [PeerTube](https://joinpeertube.org) est le logiciel que nous développons pour les créatrices, médias, institutions, enseignants… Pour gérer leur propre plateforme vidéo, comme une alternative à Youtube et Twitch.

<div class="news-card">
<p>
   <strong>🦆 VS 😈 : Reprenons du terrain aux géants du web !</strong>
</p>

<em>Grâce à <a href="https://support.joinpeertube.org">vos dons (défiscalisables à 66 %)</a>, l’association Framasoft agit pour faire avancer le web éthique et convivial. Retrouvez un résumé de nos avancées en 2023 sur le site <a href="https://support.joinpeertube.org">Soutenir Framasoft</a>.</em>

➡️ <a target="_blank" rel="nofollow noreferrer noopener" href="https://framablog.org/tag/collectivisons-internet-convivialisons-internet/">Lire la série d’articles de cette campagne</a> (nov. – déc. 2023)
</div>

La sixième version majeure est publiée aujourd'hui et nous en sommes très fier·es ! C'est la plus ambitieuse depuis l'ajout du streaming en direct et en pair-à-pair. Il y a une bonne raison à cela : nous avons rempli cette v6 de fonctionnalités inspirées par [vos idées](https://ideas.joinpeertube.org) !

![](/img/news/release-6.0/fr/3-youtube-videoraptor-light-265x300.png)

Nous sommes tellement impatient·es de vous présenter tout le travail que nous avons accompli que nous allons le faire sans introduction... mais pensez à nous suivre ! Dans deux semaines, nous prendrons plus de temps pour parler de l'histoire de PeerTube, de l'état actuel de ce projet et des grands projets que nous avons pour son avenir !

#### Cette année : deux mises à jour mineures et une avancée majeure

En 2023, et avant de préparer cette mise à jour majeure, nous n'avons publié que deux versions mineures… mais l'une d'entre elles a apporté une fonctionnalité technique majeure qui contribuera à démocratiser encore davantage l'hébergement de vidéos.

##### Mars 2023 : PeerTube v5.1

Vous trouverez plus de détails dans [la news dédiée à la version 5.1](https://joinpeertube.org/news/release-5.1), donc pour faire court, cette version apporte :
* une fonctionnalité "demander un compte", où les modérateurices d'instance peuvent **gérer et modérer les nouvelles demandes de compte** ;
* un **bouton de retour au direct**, qui vous permet de revenir au direct lorsque vous êtes à la traîne lors d'un direct ;
* Améliorations du **plugin d'identification**, pour faciliter la connexion avec des identifiants externes.

##### Juin 2023 : PeerTube 5.2...

Comme vous le découvrirez dans notre [blogpost sur la version 5.2](https://joinpeertube.org/news/release-5.2), il y a eu quelques nouvelles fonctionnalités plus petites mais importantes telles que :
* L'adaptation des **flux RSS aux standards de podcast**, de sorte que n'importe quel logiciel de podcast puisse être capable de lire une chaîne PeerTube, par exemple ;
* L'option permettant de **définir la confidentialité de la rediffusion d'un direct**, afin que les vidéastes puissent choisir à l'avance si la rediffusion de leur live sera *Publique*, *Non listée*, *Privée* ou *Interne* ;
* Amélioration de la navigation sans souris : pour celles qui préfèrent ou ceux qui doivent **naviguer à l'aide de leur clavier** ;
* Et des **améliorations de notre documentation** (elle est très complète : [consultez-la](https://docs.joinpeertube.org/) !).

##### …avec une fonctionnalité majeure : le transcodage distant

Mais ce qui a changé la donne dans cette version 5.2, c'est la nouvelle fonctionnalité de [transcodage à distance](https://docs.joinpeertube.org/admin/remote-runners).

Lorsqu'un vidéaste télécharge une vidéo (ou lorsqu'elle diffuse en direct), PeerTube doit transformer son fichier vidéo dans un format efficace. Cette tâche est appelée transcodage vidéo et consomme beaucoup de puissance de calcul (CPU). Les administratrices de PeerTube avaient besoin de gros serveurs CPU (coûteux) pour une tâche qui n'était pas permanente... jusqu'au transcodage à distance.

Le transcodage à distance permet aux administrateurs de PeerTube de déporter tout ou partie de leurs tâches de transcodage sur un autre serveur, plus puissant, qui peut être partagé avec d'autres administratrices, par exemple.

**Cela rend l'ensemble de l'administration PeerTube moins chère, plus résiliente, plus économe en énergie**... et ouvre une voie de partage des ressources entre les communautés !

Nous voulons, une fois de plus, remercier le programme NGI Entrust et la fondation NLnet pour la bourse qui nous a permis de réaliser une telle amélioration technique !

![](/img/news/release-6.0/fr/3-sepia-276x300.png)

#### PeerTube v6 est frais... grâce aux idées que vous nous avez soufflées !

Assez parlé du passé, détaillons les fonctionnalités de cette nouvelle version majeure. Notez que, pour toute cette feuille de route 2023, nous avons développé des fonctionnalités suggérées et votées par… vous ! Ou du moins par celles et ceux d'entre vous qui ont partagé leurs idées sur [notre site de suggestions (en Anglais)](https://ideas.joinpeertube.org).


##### Protégez vos vidéos avec des mots de passe !

Cette fonctionnalité était très attendue. Les vidéos protégées par un mot de passe peuvent être utilisées dans de nombreuses situations : pour créer un contenu exclusif, marquer une étape dans un parcours pédagogique, partager des vidéos avec des personnes de confiance...

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/e15b5e51-d603-41f4-b911-dcd88a651bc2?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

Sur leur compte PeerTube, les vidéastes peuvent désormais définir un mot de passe unique lorsqu'iels téléchargent, importent ou mettent à jour les paramètres de leurs vidéos.

Mais avec notre API REST, les administrateurs et les développeuses peuvent aller plus loin. Ils peuvent définir et stocker autant de mots de passe qu'elles le souhaitent, ce qui leur permet de donner et de révoquer facilement l'accès aux vidéos.

Cette fonctionnalité est le fruit du travail de Wicklow, pendant son stage chez nous.

##### Storyboard vidéo : prévisualisez ce qui va suivre !

Si vous aimez regarder vos vidéos en ligne, vous avez peut-être l'habitude de survoler la barre de progression avec votre souris ou votre doigt. Habituellement, un aperçu de l'image apparaît sous forme de vignette : c'est ce qu'on appelle le storyboard, et c'est maintenant disponible dans PeerTube !

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : Storyboard" src="https://framatube.org/videos/embed/73556243-7a9b-496c-a740-f80e42ee0ad9?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

Veuillez noter que comme les storyboards ne sont générés que lors du téléchargement (ou de l'importation) d'une vidéo, ils ne seront donc disponibles que pour les nouvelles vidéos des instances qui sont passées à la v6...

Ou vous pouvez demander, très gentiment, à vos administrateurs d'utiliser la commande magique `npm run create-generate-storyboard-job` (attention : cette tâche peut nécessiter un peu de puissance CPU), afin de générer des storyboards pour les anciennes vidéos.

##### Téléchargez une nouvelle version de votre vidéo !

Parfois, les créateurs de vidéos veulent mettre à jour une vidéo, pour corriger une erreur, offrir de nouvelles informations… ou simplement pour proposer un meilleur montage de leur travail !

Désormais, avec PeerTube, elles peuvent télécharger et remplacer une ancienne version de leur vidéo. Bien que l'ancien fichier vidéo soit définitivement effacé (pas de retour en arrière !), les créatrices conservent la même URL, le titre et les informations, les commentaires, les statistiques, etc.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : reupload video" src="https://framatube.org/videos/embed/aeb01797-5adf-4297-90dc-c927c63eef08?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>


Il est évident qu'une telle fonctionnalité nécessite de la confiance des vidéastes et des administrateurs, qui ne veulent pas être responsables de la "mise à jour" d'une adorable vidéo de chatons en une horrible publicité pour des groupes de discrimination contre les chats.

C'est pourquoi une telle fonctionnalité ne sera disponible que si les administratrices choisissent de l'activer sur leurs plateformes PeerTube, et affichera la date où le fichier a été remplacé sur les vidéos mises à jour.

##### Ajoutez des chapitres à vos vidéos !

Les vidéastes peuvent désormais ajouter des chapitres à leurs vidéos sur PeerTube. Dans la page des paramètres de la vidéo, ils obtiendront un nouvel ongle "chapitres" où elles n'auront qu'à spécifier le timecode et le titre de chaque chapitre pour que PeerTube l'ajoute.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : chapters" src="https://framatube.org/videos/embed/6f0feeeb-cade-47d8-bfbf-a9a8504efdf3?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

S'ils importent leur vidéo depuis une autre plateforme (*tousse* YouTube *tousse*), PeerTube devrait automatiquement reconnaître et importer les chapitres définis sur cette vidéo distante.

Lorsque des chapitres sont définis, des marqueurs apparaissent et segmentent la barre de progression. Les titres des chapitres s'affichent lorsque vous survolez ou touchez l'un de ces segments.

##### Tests de charge, performances et recommandations de configuration

L'année dernière, grâce à l'émission « Au Poste ! » du journaliste français David Dufresne et à son hébergeur Octopuce, nous avons eu droit à un test de charge du direct avec plus de 400 spectateurices simultanés : [voir le rapport ici sur le blog d'Octopuce](https://www.octopuce.fr/test-de-charge-dun-peertube-en-live-avec-auposte/).


De tels tests sont vraiment utiles pour comprendre où nous pouvons améliorer PeerTube pour réduire les goulots d'étranglement, améliorer les performances, et donner des conseils sur la meilleure configuration pour un serveur PeerTube si un administrateur prévoit d'avoir beaucoup de trafic.


C'est pourquoi cette année, nous avons décidé de réaliser plus de tests, avec un millier d'utilisateurs simultanés simulés à la fois dans des conditions de direct et de diffusion de vidéo classique. Nous remercions Octopuce de nous avoir aidé·es à déployer notre infrastructure de test.


Nous publierons bientôt un rapport avec nos conclusions et les configurations de serveurs recommandées en fonction des cas d'utilisation (fin 2023, début 2024). En attendant, les premiers tests nous ont motivés à **ajouter de nombreuses améliorations de performances** dans cette v6, telles que (préparez-vous aux termes techniques) :
* Traiter les tâches HTTP unicast dans les worker threads
* Signer les requêtes ActivityPub dans les worker threads
* Optimisation des requêtes HTTP pour les vidéos recommandées
* Optimisation des requêtes SQL pour les vidéos lors du filtrage sur les directs ou les tags
* Optimiser les endpoints /videos/{id}/views avec de nombreux spectateurs
* Ajout de la possibilité de désactiver les journaux HTTP de PeerTube


##### ...et il y en a toujours plus !


Une nouvelle version majeure s'accompagne toujours de son lot de changements, d'améliorations, de corrections de bogues, etc. Vous pouvez lire [le journal complet ici (en Anglais)](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.0), mais en voici les grandes lignes :


* Nous avions besoin de régler une dette technique : **la version 6 supprime la prise en charge de WebTorrent pour se concentrer sur HLS (avec P2P via WebRTC)**. Les deux sont des briques techniques utilisées pour diffuser en pair à pair dans les navigateurs web, mais HLS est plus adapté à ce que nous faisons (et prévoyons de faire) avec PeerTube
* Le lecteur vidéo est plus efficace
  * Il n'est plus reconstruit à chaque fois que la vidéo change ;
  * Il conserve vos paramètres de visionnage (vitesse, plein écran, etc.) lorsque la vidéo change ;
  * Il ajuste automatiquement sa taille en fonction du ratio de la vidéo ;
* Nous avons amélioré le référencement, pour aider les vidéos hébergées sur une plateforme PeerTube à apparaître plus haut dans les résultats des moteurs de recherche ;
* Nous avons beaucoup travaillé sur l'amélioration de l'accessibilité de PeerTube à plusieurs niveaux, afin de simplifier l'expérience des personnes en situation de handicap.

![](/img/news/release-6.0/fr/5-youtube-premium-yetube-light-279x300.png)


#### Qu'en est-il de l'avenir de PeerTube ?

Alors que YouTube fait la guerre aux bloqueurs de publicité, que Twitch exploite de plus en plus les vidéastes et que tout le monde est de plus en plus conscient de la toxicité de ce système, PeerTube est en train de gagner du terrain, est de plus en plus reconnu et voit sa communauté grandir.

Nous avons tellement d'annonces à faire sur l'avenir que nous prévoyons pour PeerTube, que nous publierons une annonce séparée, dans deux semaines. Nous prévoyons également d'organiser un direct, afin de répondre aux questions que vous vous posez sur PeerTube.

Vous resterez au courant en vous abonnant à la [Lettre d'information de PeerTube](https://joinpeertube.org/news), en suivant le [compte Mastodon de PeerTube](https://framapiaf.org/@peertube) ou en surveillant le [Framablog](https://framablog.org).


[![](/img/news/release-6.0/fr/sepia-1024x576.jpg "Cliquez pour nous soutenir et aider Sepia à repousser Videoraptor – Illustration CC-By David Revoy")](https://support.joinpeertube.org/)

#### Merci de soutenir PeerTube et Framasoft


En attendant, nous voulons vous rappeler que tous ces développements ont été réalisés par un seul développeur rémunéré, un stagiaire, et une fabuleuse communauté (beaucoup de datalove à Chocobozzz, Wicklow, et les nombreuses, nombreux contributeurs : vous êtes toustes incroyables !)

Framasoft étant une association française à but non lucratif principalement financée par des dons (75% de nos revenus annuels proviennent de personnes comme vous et nous), le développement de PeerTube a été financé par deux sources principales :
* les francophones sensibilisées aulogiciel libre
* Les subventions de l'initiative Next Generation Internet, par l'intermédiaire de NLnet (en 2021 et 2023).


Si vous êtes un afficionado non francophone de PeerTube, merci de **soutenir notre travail en [faisant un don à Framasoft](https://support.joinpeertube.org)**. Cela nous aidera grandement à financer nos très nombreux projets, et à équilibrer notre budget 2024.


Cette année encore, nous avons besoin de vous, de votre soutien, de votre partage pour nous aider à regagner du terrain sur le web toxique des GAFAM et à multiplier le nombre d'espaces numériques éthiques. Nous avons donc demandé à [David Revoy](https://www.peppercarrot.com/fr/files/framasoft.html) de nous aider à présenter cela sur notre page [support Framasoft](https://support.joinpeertube.org), que nous vous invitons à visiter (parce qu'elle est belle) et surtout à partager le plus largement possible :

[![Capture d'écran de la barre de dons Framasoft 2023 à 12% - 23575 €](/img/news/release-6.0/fr/donation.png)](https://support.joinpeertube.org)

**Si nous voulons équilibrer notre budget pour 2024, nous avons cinq semaines pour collecter 176 425 € : nous ne pouvons pas le faire sans votre aide !**

<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" rel="nofollow noreferrer noopener" href="https://support.joinpeertube.org/">
      Soutenir Framasoft
   </a>
</div>

Merci encore de soutenir PeerTube,
l'équipe de Framasoft.
