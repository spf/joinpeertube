---
id: peertube-future-2024
title: Application mobile, redesign, nouveau développeur, promotion... construisons un avenir radieux pour PeerTube !
date: December 12, 2023
image: https://framablog.org/wp-content/uploads/2023/11/sepia-1024x576.jpg
---

Développer une alternative éthique et émancipatrice à YouTube, Twitch ou Vimeo sans les moyens du capitalisme de surveillance est une entreprise gigantesque. Surtout pour une petite association française à but non lucratif qui gère déjà plusieurs projets de promotion des biens communs numériques.

<div class="news-card">
<p>
   <strong>🦆 VS 😈 : Reprenons du terrain aux géants du web !</strong>
</p>

<em>Grâce à <a href="https://support.joinpeertube.org">vos dons (défiscalisables à 66 %)</a>, l’association Framasoft agit pour faire avancer le web éthique et convivial. Retrouvez un résumé de nos avancées en 2023 sur le site <a href="https://support.joinpeertube.org">Soutenir Framasoft</a>.</em>

➡️ <a target="_blank" rel="nofollow noreferrer noopener" href="https://framablog.org/tag/collectivisons-internet-convivialisons-internet/">Lire la série d’articles de cette campagne</a> (nov. – déc. 2023)
</div>

Cela fait six ans que nous développons PeerTube. Deux semaines après [la sortie de la sixième version du logiciel](https://framablog.org/2023/11/28/peertube-v6-est-publie-et-concu-grace-a-vos-idees/), prenons un peu de recul sur ces six années de travail, examinons l’immense opportunité que représente la période actuelle pour PeerTube, et regardons ce que nous comptons faire l’année prochaine pour préparer son succès… si vous nous donnez [les moyens d’y arriver](https://support.joinpeertube.org/) !

[![Illustration de Yetube, un monstre de type Yéti avec le logo de YouTube Premium.](https://framablog.org/wp-content/uploads/2023/11/5-youtube-premium-yetube-light-279x300.png "Cliquez pour soutenir Framasoft et repousser le Yetube – Illustration CC-By David Revoy")](https://support.joinpeertube.org)


#### Pas un rival, juste une alternative

Le constat qui nous a amenés à développer PeerTube est que personne ne peut rivaliser avec YouTube ou Twitch. Vous auriez besoin de l’argent de Google, des fermes de serveurs d’Amazon… Par-dessus tout, **vous auriez besoin de la cupidité nécessaire pour exploiter des millions de créateurs et de vidéastes**, les préparer à formater leur contenu en fonction de vos besoins, et les nourrir des miettes de **la richesse que vous gagnez en transformant leur audience en bétail de données**.

Les plateformes vidéo centralisées et monopolistiques ne peuvent être maintenues que par le capitalisme de surveillance.

Nous voulions que les petits groupes tels que les institutions, les éducateurs, les communautés, les artistes, les citoyens, etc. aient les moyens de s’émanciper des plateformes de Big Tech, sans se perdre dans le World Wide Web. Nous avions besoin de développer pour démocratiser l’hébergement vidéo, il fallait donc le concevoir avec des valeurs radicalement différentes à l’esprit.

> Et c’est ce que nous avons fait. Nous construisons PeerTube pour donner du pouvoir aux gens, et non aux bases de données ou aux actionnaires.

Aujourd’hui, PeerTube est :

*   un **logiciel libre** (transparence, protection contre les monopoles)
*   vous pouvez l’héberger **sur votre serveur** (self-hosting, autonomie, empowerment)
*   de créer votre plateforme vidéo et de diffusion en direct, **avec vos propres règles** (création d’une communauté, autogestion)
*   qui vous permet de vous **fédérer** (ou non !) à d’autres plateformes PeerTube via le protocole ActivityPub (fédération, réseau, diffusion)
*   qui ajoute le **streaming pair-à-pair** (optionnel) au streaming classique afin qu’il puisse résister à l’abondance (résilience, partage, décentralisation)
*   où les serveurs les plus puissants peuvent aider les moins chanceux grâce à la **redondance** (solidarité, résilience)
*   qui peut **stocker des vidéos en externe** grâce au stockage S3 (adaptabilité, rentabilité)
*   qui peut **déporter sur un serveur dédié les tâches gourmandes en ressources processeur** telles que le transcodage vidéo ou en direct (efficacité, résilience, durabilité)

Donc non : PeerTube n’est pas et ne sera pas un rival de YouTube ou de Twitch. **PeerTube est alimenté par d’autres valeurs qui celles codées dans les écosystèmes de Google et d’Amazon**. PeerTube est une alternative, et c’est exactement pour cela que c’est si excitant.

[![Dessin de Sepia, læ poulpe mascotte de PeerTube. Iel porte une cape de super héros, avec le sigle "6" sur son torse.](https://framablog.org/wp-content/uploads/2023/11/3-sepia-276x300.png "Cliquez pour soutenir Sepia – illustration David Revoy – licence : CC-By 4.0")](https://support.joinpeertube.org)


#### PeerTube est un logiciel : 6 ans de développements

Au cours des six dernières années, avec plus de 275 000 lignes de code, nous avons obtenu :

*   D’une preuve de concept à une plateforme vidéo fédérée pleinement opérationnelle avec diffusion paire-à-paire, complète avec sous-titres, redondance, importation de vidéos, outils de recherche et localisation ([PeerTube v1, oct. 2018](https://framablog.org/2018/10/15/peertube-1-0-la-plateforme-de-videos-libre-et-federee/))
*   Des notifications, des listes de lecture, un système de plugins, des outils de modération, des outils de fédération, un meilleur lecteur vidéo, un site web de présentation et un index des instances ([PeerTube v2, nov. 2019](https://framablog.org/2019/11/12/peertube-met-les-bouchees-doubles-pour-emanciper-vos-videos-de-youtube/))
*   D’un outil de recherche fédérée (et un moteur de recherche https://sepiasearch.org), plus d’outils de modération, beaucoup d’améliorations du code, une refonte de l’UX, et enfin : diffusion en direct en pair-à-pair ([PeerTube v3, Jan. 2021](https://framablog.org/2021/01/07/peertube-v3-ca-part-en-live/))
*   L’amélioration du transcodage, de la personnalisation de la page d’accueil des chaînes et des instances, recherche améliorée, lecteur vidéo encore plus performant, filtrage des vidéos sur les pages, outils d’administration et de modération avancés, nouvel outil de gestion des vidéos, et une grande session de nettoyage du code ([PeerTube v4, déc. 2021](https://framablog.org/2021/11/30/peertube-v4-prenez-le-pouvoir-pour-presenter-vos-videos/))
*   Un outil d’édition vidéo, un affichage amélioré des statistiques et des mesures vidéo, une fonction de relecture pour les diffusions en direct permanentes, des paramètres de latence pour les lives, un lecteur vidéo amélioré (pour les écrans mobiles), un système de plugins plus puissant, davantage d’options de personnalisation, davantage d’options de filtrage vidéo, un nouvel outil convivial pour proposer des idées et un site web de présentation renouvelé ([PeerTube v5, déc. 2022](https://framablog.org/2022/12/13/peertube-v5-le-resultat-de-5-ans-de-travail-artisanal/))
*   La modération des demandes de compte, un bouton de retour au direct, transcodage à distance (pour déporter la tâche gourmande en CPU sur un serveur dédié). Storyboard (prévisualisation dans la barre de progression), chapitres vidéo, accessibilité améliorée, téléversement d’une nouvelle version d’une vidéo, et vidéos protégées par un mot de passe. ([PeerTube v6, Nov. 2023](https://framablog.org/2023/11/28/peertube-v6-est-publie-et-concu-grace-a-vos-idees/))

Et ce n’est que **la partie développement logiciel de PeerTube**. Pour soutenir et promouvoir ce logiciel, nous avons dû construire tout un écosystème.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : chapters" src="https://framatube.org/videos/embed/6f0feeeb-cade-47d8-bfbf-a9a8504efdf3?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

#### PeerTube est aussi un écosystème

PeerTube, aujourd’hui, est aussi une **communauté de développeur·euses**. Sur la [forge du projet](https://github.com/Chocobozzz/PeerTube/) (espace en ligne pour contribuer aux développements), nous avons eu plus de 400 contributeurs et contributrices, 4 300 problèmes (fonctionnalités et demandes de support) fermés en 6 ans et 500 toujours ouverts, et 12 400 contributions intégrées en amont.

Comme tout le monde ne peut pas se familiariser avec plus de 275 000 lignes de code, un moyen facile de **contribuer à PeerTube est de développer des plugins** : il y en a des centaines ! Parmi eux, il y a le chat en direct (pour obtenir un chat pendant les diffusions en direct), des plugins pour s’authentifier auprès de plateformes d’authentification externes, des annotations à ajouter dans le lecteur vidéo, un plugin de transcription pour créer automatiquement des sous-titres pour vos vidéos ou encore des plugins pour ajouter de la monétisation aux vidéos de PeerTube.

Les contributeurs et contributrices ont également aidé en **traduisant PeerTube** dans plus de 36 langues ([rejoignez-les ici](https://weblate.framasoft.org/projects/peertube/#languages)), en fournissant des réponses [sur notre forum](https://framacolibri.org/c/peertube/peertube/37), en mettant à jour notre [documentation officielle](https://docs.joinpeertube.org/), ou en partageant des idées sur [notre outil de demandes améliorations PeerTube](https://ideas.joinpeertube.org/) (en anglais).

Il y a maintenant plus d’un millier de plateformes PeerTube dans le monde (à notre connaissance ^^), hébergeant près d’un million de vidéos. Nous avons créé [un index d’instances](https://instances.joinpeertube.org/instances) qui alimente [SepiaSearch](https://sepiasearch.org/), notre **moteur de recherche pour les vidéos, chaînes et listes de lecture PeerTube**. Nous le modérons selon nos termes et conditions, mais chacun⋅e est libre d’utiliser le code que nous développons pour créer son propre [index](https://framagit.org/framasoft/peertube/instances-peertube) et son propre [moteur de recherche](https://framagit.org/framasoft/peertube/search-index).

Heureusement, d’autres personnes travaillent à **la promotion et à la modération du contenu de PeerTube**, en créant [des annuaires](https://peertube-annuaire.nogafam.fr/), des [fils de recommandations](https://social.growyourown.services/@FediFollows/111291322079656821) (en anglais), des [outils de modération](https://peertube_isolation.frama.io/), des [extensions Firefox](https://addons.mozilla.org/fr/firefox/addon/peertube-companion/), et toutes sortes de contenus étonnants.

Nous **promouvons PeerTube** avec un site officiel Joinpeertube.org, où les dernières nouvelles sont partagées sur [le blog et la newsletter](https://joinpeertube.org/news). Il y a également un [compte Mastodon](https://framapiaf.org/deck/@peertube) (et [un compte – presque abandonné – sur Twitter](https://twitter.com/joinpeertube)). Nous passons également de nombreuses heures à discuter avec les médias, les chercheuses, les innovateurs, les communautés, les contributeurs et contributrices, etc.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : Storyboard" src="https://framatube.org/videos/embed/73556243-7a9b-496c-a740-f80e42ee0ad9?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

#### Combattre les dragons avec des cure-dents

Alors, **comment estimer le coût de ces 6 années de travail** ? Doit-on considérer uniquement le temps de développement et la gestion de la communauté de développement (problèmes, revue de code, support) ?

Faut-il aussi compter le travail effectué sur les articles de blog, les illustrations et le matériel de promotion, l’établissement des feuilles de route, le travail avec les designers, l’échange d’expérience avec les chercheur·euses, les vidéastes, et les [projets étonnants](https://bunseed.org/), dont certains que [nous avons soutenus financièrement](https://github.com/JohnXLivingston/peertube-plugin-livechat/releases/tag/v8.0.0) ? Qu’en est-il du temps consacré à la modération de notre [moteur de recherche](https://sepiasearch.org/) ou à la lutte contre les spammeurs sur notre [outil de proposition](https://ideas.joinpeertube.org/) ?

Même si nous ne pouvons pas préciser le budget exact que Framasoft a consacré à PeerTube depuis 2017, **notre estimation prudente se situerait autour de 500 000 €**. Sur six ans. Comme nous avons obtenu deux subventions de la Commission européenne (via les programmes NGI0 [Search & Discovery](https://nlnet.nl/project/PeerTube/) et [Entrust](https://nlnet.nl/project/Peertube-Transcode/)) pour un total de 132 000 €, cela signifie que 73,6 % du budget de PeerTube provient de dons.

Maintenant, **surestimons le coût de PeerTube à 600 000 €** sur 6 ans, pour nous assurer que nous avons couvert toutes les dépenses.

**Même dans ce cas, le coût total de PeerTube représenterait 22 millionièmes (0,0022 %) des recettes publicitaires de YouTube l’année dernière. Oui, nous avons fait le calcul.**

_([source](https://www.statista.com/statistics/289658/youtube-global-net-advertising-revenues/) – 29.243 B USD // 632 853 USD)_

Nous nous battons – au sens figuré – contre des dragons avec des cure-dents. C’est pourquoi nous pensons que PeerTube ne peut pas rivaliser et ne rivalisera pas avec YouTube ni avec Twitch (et encore moins avec TikTok qui présente une toute autre expérience).

Mais, en tant qu’alternative, PeerTube est déjà un succès.

[![Dessin dans le style d'un jeu vidéo de combat, où s'affronte le poulpe de PeerTube et le monstre de YouTube, Twitch et Viméo.](https://framablog.org/wp-content/uploads/2023/11/sepia-1024x576.jpg "Cliquez pour soutenir Sepia contre le Videoraptor – illustration David Revoy – licence : CC-By 4.0")](https://support.joinpeertube.org)

#### Un succès à nos yeux

Aujourd’hui, nous connaissons plus de 1000 instances (serveurs sur lesquels PeerTube est installé et fonctionne), partageant près d’un million de vidéos.

N’étant pas limité par [la mécanique de captation](https://fr.wikipedia.org/wiki/Captologie) d’un modèle basé sur la publicité et l’attention, **PeerTube offre des fonctionnalités qui ne sont pas disponibles chez les géants de la technologie** :

*   **compatibilité avec d’autres outils sociaux** via [ActivityPub](https://fr.wikipedia.org/wiki/ActivityPub) (Imaginez que vous puissiez tweeter un commentaire sur une vidéo YouTube : avec Mastodon et PeerTube, c’est possible)
*   **partager une vidéo** d’un timecode de départ à un **timecode d’arrêt** (YouTube nous a rattrapés, depuis)
*   **un accès chronologique ininterrompu à votre flux d’abonnements** (pas besoin de « cliquer sur la cloche » en plus de l’abonnement)
*   **vidéos protégées par un mot de passe** (indisponibles sur YouTube, payantes sur Vimeo)
*   **remplacer une vidéo** par une version actualisée

Nous avions l’intention de créer PeerTube spécifiquement pour les personnes qui ont besoin (et veulent) **partager leurs vidéos en dehors du modèle du capitalisme de surveillance**. Il est évident que nous connaissons tous⋅tes (et apprécions) certains vidéastes Youtube et Twitch, mais iels ne représentent que la partie visible de l’iceberg du partage de vidéos en ligne.


<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/aeb01797-5adf-4297-90dc-c927c63eef08?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

Les institutions, les éducateurs, les médias indépendants, les citoyens et même les créateurs devraient avoir la liberté de partager des vidéos en ligne sans contribuer au monopole d’une entreprise, sans avoir à accepter des publicités forcées ou sans sacrifier les données et la vie privée de leur public. La bonne nouvelle, c’est que certains d’entre eux ont déjà trouvé cette liberté, et nous en sommes fiers :

*   **Institutions**
    *   [La commission européenne](https://tube.network.europa.eu/)
    *   [Netherlands Institute for sound and vision](https://peertube.beeldengeluid.nl)
*   **Education**
    *   [La plateforme logiciels libres](https://tubes.apps.education.fr/) du ministère de l’éducation
    *   University of Philippines Diliman’s [digital learning programs](https://stream.ilc.upd.edu.ph)
*   **Médias indépendants**
    *   [Blast](https://video.blast-info.fr/) (Média en ligne français indépendant de gauche)
    *   [Howlround](https://howlround.com) (Theater Commons media situé à l’Emerson College, Boston)
*   **Citoyens et citoyennes**
    *   Urbanists.video (vidéos sur les lieux où l’on peut marcher et vivre)
    *   S2S (espace sécurisé pour les personnes sourdes et malentendantes, vidéos sur la langue des signes française)
    *   [Live it live](https://liveitlive.show) (concerts de musique en direct)
*   **Créateurs et créatrices**
    *   [Skeptikon](https://skeptikon.fr) (collectif français, vidéos sur l’esprit critique et le scepticisme)
    *   [TILvids](https://tilvids.com/) (TIL = Today I Learned (aujourd’hui j’ai appris), vidéos ludo-éducatives en anglais, avec miroir autorisé et officiel de YouTube)
    *   [Bunseed](https://bunseed.org/) (initiative française, alternative à Patreon basée sur le logiciel libre, par et pour les créateurs, basée sur PeerTube)

Nous voulons tirer parti de la reconnaissance dont jouit PeerTube, c’est pourquoi nous avons prévu beaucoup de travail pour 2024 !

<div class="mt-3 mb-3" style="margin: auto;">
  <div style="position: relative; padding-top: 56.25%;">
    <iframe title="Peertube presentation at NGI forum 2023 - by Pouhiou" width="100%" height="100%" src="https://framatube.org/videos/embed/5ddc8a25-33be-4a93-b710-bef1b6145d4e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;"></iframe>
  </div>
</div>

#### La feuille de route de PeerTube vers la v7, en 2024

Les fonctionnalités que nous avons prévues pour la prochaine année de développement de PeerTube ont toutes le même objectif : faciliter l’adoption en améliorant la facilité d’utilisation de plusieurs façons. Comme pour la version 6, la plupart de ces fonctionnalités ont été choisies à partir des idées que vous avez partagées et pour lesquelles vous avez voté sur [notre outil de proposition](https://ideas.joinpeertube.org/).

Nous prévoyons de :

*   Ajouter un système d’**export/import des données d’un compte** (avec ou sans fichiers vidéo), afin que les utilisateurs et utiliastrices puissent facilement changer d’instance.
*   Réaliser un **audit d’accessibilité complet**, afin de faciliter l’utilisation pour les personnes ayant des besoins spécifiques, et compléter le travail effectué cette année (voir la version 6). S’il nous reste du temps pour intégrer les recommandations du rapport, nous verrons si et comment nous pourrions ajouter la transcription de l’audio en texte.
*   Ajouter un **outil de modération des commentaires** utilisable à la fois par les administratrices d’instances et les vidéastes.
*   Créer un **nouvel outil de modération** pour trier le contenu en fonction de listes de mots-clés prédéfinies (« mots-clés de l’extrême droite en allemand », « injures queerphobes en anglais », etc.). Cet outil présentera les contenus correspondants aux administrateurs et modératrices des instances, qui détermineront alors s’ils correspondent à leur politique de modération.
*   Organiser la **séparation (technique) des flux audio et vidéo**. Cette amélioration permettra, à l’avenir, de développer et d’obtenir des vidéos à pistes audio multiples (par exemple, en plusieurs langues), ou des vidéos à pistes multiples avec le même flux audio (par exemple, sous plusieurs angles).
*   Ajouter **une nouvelle résolution « audio »** (dans le menu « 720p », « 1080p », etc.) pour notre lecteur HLS. Cela permettra aux utilisatrices de ne recevoir que la piste audio, améliorant ainsi la durabilité lorsqu’ils veulent seulement écouter une vidéo et regarder d’autres onglets.
*   Repenser la **caractérisation du contenu sensible**. À l’heure actuelle, vous ne pouvez étiqueter les vidéos que comme « Safe for work » / « Not Safe For Work ». Or, le terme « contenu sensible » peut recouvrir de nombreux cas : violence, nudité, jurons, etc. Nous travaillerons avec des designers pour réfléchir à la manière appropriée de catégoriser et de traiter ces cas.
*   **Réorganiser l’espace de gestion des vidéos**. Nous avons ajouté beaucoup de nouvelles fonctionnalités au fil des ans (direct et rediffusion, studio d’édition de vidéo, etc.)… c’est bien, mais les onglets et les menus se sont accumulés. Nous travaillerons avec des designers pour repenser le système de A à Z et le rendre plus facile à utiliser.
*   Procéder à un examen complet et mettre en œuvre une **refonte de l’expérience et de l’interface de PeerTube**. Même si nous avons reçu beaucoup d’aide en cours de route, PeerTube n’a pas bénéficié d’un suivi en design dès le départ. Nous voulons considérer ce chantier comme une remise à plat, où tout (même la couleur orange ?) peut être remis en question, si cela aide à l’adoption et à la facilité d’utilisation.

[![Illustration de Videoraptor, un monstre insectoïde dont les trois têtes sont ornées des logos de YouTube, Viméo et Twitch](https://framablog.org/wp-content/uploads/2023/11/3-youtube-videoraptor-light-265x300.png "Aidez-nous à repousser le Videoraptor – Illustration CC-By David Revoy")](https://support.joinpeertube.org)

#### Doubler l’équipe de développement pour plus de résilience…

D’accord, quand on passe d’un à deux développeurs, c’est facile de « doubler »… mais c’était quand même une grande question pour nous.

D’abord parce que **Framasoft est une association à but non lucratif [financée principalement par des dons](https://support.joinpeertube.org)**. Jusqu’à présent, nous avons eu l’honneur et le privilège d’obtenir suffisamment de soutien pour financer nos dépenses, la principale étant de rémunérer nos 10 employé·es. Mais les modèles économiques basés sur les dons sont, par définition, hautement imprévisibles. C’est particulièrement vrai dans une économie où l’inflation, les coûts de l’énergie, etc. poussent la plupart de nos donateurs et donatrices à revoir leur budget.

Une autre raison réside dans [nos valeurs fondamentales](https://framasoft.org/fr/manifest) : **nous croyons à la décentralisation et aux réseaux de petites actrices** (plutôt qu’à la croissance des géants et des monopoles). Nous pensons également que donner la priorité à l’humain et au soin implique de **rester dans une petite équipe à taille humaine**, où nous nous connaissons vraiment les uns les autres.

Or nous pensons que la manière dont nous avons appliqué ces valeurs dans notre association est une des clés de l’efficacité, de la créativité et des talents exprimés par nos membres (bénévoles et employé·es). C’est pourquoi nous avons travaillé à limiter la croissance de Framasoft, en nous fixant une limite symbolique de « dix salarié⋅es maximum ».

Au cours des années 2022 et 2023, ce sujet a fait l’objet de nombreuses discussions au sein de Framasoft. D’une part, on ne peut pas continuer à développer PeerTube avec un seul développeur (même si c’est un développeur aussi talentueux que Chocobozzz), qui peut gagner au loto, partir, ou tout simplement changer de carrière. D’autre part, si nous embauchions un nouveau développeur, quel serait son profil ? Comment pouvons-nous nous assurer qu’elle s’intégrera ? Pouvons-nous lui assurer un emploi durable ?

**Fin 2022, Chocobozzz nous a demandé de publier une offre de stage**. Il s’agissait à la fois de tester si, après 5 ans de développement en solo sur PeerTube, le travail en équipe lui revenait facilement (c’est le cas) ; mais aussi de former quelqu’un au code de PeerTube, de voir comment il peut être appréhendé par une nouvelle personne, et comment améliorer sa documentation.

Wicklow nous a rejoint pour un stage entre février et août 2023, et a produit la fonctionnalité de protection de vidéos par mot de passe, publiée dans la [version 6 de PeerTube](https://framablog.org/2023/11/28/peertube-v6-est-publie-et-concu-grace-a-vos-idees/). **Nous n’avions pas prévu de l’embaucher** : nous avions alors d’autres profils en tête, et pensions ne pas pouvoir lancer un processus d’embauche avant 2024. Nous le lui avons dit expressément, pour ne pas lui donner de faux espoirs… Mais au même moment où nous apprenions pouvoir bénéficier d’une [extension de bourse du programme NGI0](https://nlnet.nl/project/Peertube-Transcode/), nous avons réalisé qu’il s’intégrait parfaitement au projet, à l’équipe et à notre association.

**Bref : nous avons embauché Wicklow en septembre 2023**, alors qu’il venait d’obtenir son diplôme, pour un contrat d’un an (que nous espérons pérenniser [avec votre soutien](https://support.joinpeertube.org) !).

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/e15b5e51-d603-41f4-b911-dcd88a651bc2?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

#### …et pour créer une application mobile iOS/Android !

Cette nouvelle embauche a deux objectifs. Tout d’abord, nous voulons qu’un autre développeur, ou qu’une autre développeuse, se familiarise avec le code de base de PeerTube, et **réduise le « [bus factor](https://fr.wikipedia.org/wiki/Facteur_d%27autobus) »**. Wicklow devrait également devenir progressivement capable d’aider Chocobozzz dans la gestion de la communauté de développement.

Au fur et à mesure que la communauté grandit (et nous en sommes ravies), la charge de travail d’animation augmente également : répondre aux [_issues_](https://github.com/Chocobozzz/PeerTube/issues/) et aux demandes d’assistance [sur notre forum](https://framacolibri.org/c/peertube/38), examiner les contributions en code, etc. Bien qu’il soit important d’être présent pour la communauté, cela prend jusqu’à la moitié du temps de Chocobozzz, ce qui signifie encore moins de temps pour développer de nouvelles fonctionnalités.

Le deuxième et principal objectif pour Wicklow en 2024 serait, avec l’aide de designers, de **créer et de publier une application mobile PeerTube officielle**. Le visionnage mobile est devenu le principal moyen de regarder des vidéos. Même s’[il existe déjà des applications mobiles permettant de lire des vidéos sur PeerTube](https://docs.joinpeertube.org/use/third-party-application), nous pensons qu’une application officielle pourrait contribuer à l’adoption et à l’attractivité de PeerTube.

Pour 2024, l’application se limiterait à la recherche et au visionnage de vidéos. Nous voulons que les utilisatrices puissent utiliser un moteur de recherche fédéré, regarder des vidéos et des directs, se connecter à leur compte sur leur instance PeerTube, accéder à leurs notifications, abonnements, listes de lecture, etc. En cas de succès, cette première version de l’application pourrait être étendue à d’autres cas d’usage et fonctionnalités à l’avenir.

Nous prévoyons de **publier cette application à la fois sur iOS** (ce qui dépendra aussi d’Apple, connue pour être [tatillonne avec le fediverse](https://laurenshof.online/owncast-and-the-app-store/)) **et sur Android**… et, en tant qu’objectif bonus (donc « si tout se passe bien »), sur Android TV également.

[![Dessin de Sepia, læ poulpe mascotte de PeerTube. Iel est en position de méditation et entouré d'une aura de force, évoquant le super sayans.](https://framablog.org/wp-content/uploads/2023/11/5-super-sepia-196x300.png "Sepia, la mascotte de PeerTube, forte de votre soutien – illustration David Revoy – licence : CC-By 4.0")](https://support.joinpeertube.org)

#### Promouvoir l’écosystème PeerTube

PeerTube, c’est plus que du code, et nous voulons **mettre en lumière l’incroyable communauté qui se développe autour de ce projet**.

Nous voyons souvent des plugins étonnants, des instances et des chaînes intéressantes, de nouvelles initiatives et expériences… que nous aimerions partager. Mais il est rare que nous ayons et prenions le temps de le faire.

En attendant, nous voyons aussi beaucoup de gens qui se demandent si PeerTube permet la diffusion en direct (c’est le cas !), s’il y a un chat pour les lives (oui : c’est un plugin génial !), ou s’il y a des sites web pour trouver du contenu sur PeerTube (encore une fois : oui !).

Nous prévoyons de travailler à la **promotion de l’écosystème PeerTube**, grâce [au blog et à la newsletter](https://joinpeertube.org/news) de notre site [Joinpeertube](https://joinpeertube.org/), avec [notre compte Mastodon](https://framapiaf.org/@peertube), et en travaillant sur une instance vitrine [Peer.tube](https://peer.tube/).

Pour inaugurer ce travail, **nous répondrons en Anglais et en direct à toutes vos questions sur PeerTube lors d’un livestream** animé par Laurens du blog et de la newsletter Fediverse Report, sur notre chaîne Peer.Tube ! Vous pouvez déjà aller sur Mastodon et poser vos questions (en Anglais aussi) avec le hashtag #PeerTubeAMA.

Cet AMA ( » Ask Me Anything « ) aura lieu demain, 13 décembre, de 18h à 20h (CET), [sur ce lien](https://peer.tube/w/f6jxvT1WZzsBRHJF6t6saD).

[![La vignette indique "Livestream #PeerTubeAMA - Dec. 13th - 6-8pm CET"](https://framablog.org/wp-content/uploads/2023/12/PeerTube_AMA6-1024x709.png "Cliquez sur l’image pour accéder au live")](https://peer.tube/w/f6jxvT1WZzsBRHJF6t6saD)

(et si tout se passe bien, nous publierons le replay sur [la même chaîne](https://peer.tube/c/peertube_news/videos))

Si vous êtes résolument francophones, on vous donne rendez-vous le 19 décembre au matin, où nous passerons [Au Poste ! pour une PeerTube Party organisée par le journaliste David Dufresne](https://www.auposte.fr/convocation/peertube-party-bilan-perspectives-avec-framasoft/).

#### Financé par l’Europe… et par vous !

Comme nous l’avons déjà dit dans ce (long) billet, nous avons eu la chance d’obtenir des bourses du programme NGI (Next Generation Internet) de la Commission Européenne, par l’intermédiaire de la [fondation NLnet](https://nlnet.nl/) (merci beaucoup à elles et eux !). Les bourses précédentes nous ont permis de financer un quart de nos six années de travail sur PeerTube. Nous sommes heureuses d’annoncer que nous avons obtenu [une nouvelle bourse pour 2024](https://nlnet.nl/project/PeerTube-mobile/), qui couvrira les coûts de développement prévus.

Cela signifie que, comme cela a été le cas pour 75 % du travail jusqu’à présent, le financement de tout le reste du projet repose sur les dons. Communiquer sur PeerTube et son écosystème, les partages d’expérience avec divers acteurs, les prestations en design, le soutien et la gestion de la communauté, etc. Tous ces coûts seront, comme d’habitude, [financés par… certaines d’entre vous](https://support.joinpeertube.org) !

**Notre campagne de dons actuelle déterminera le budget de Framasoft pour 2024**. Son succès nous indiquera si nous pourrons assurer un emploi stable à notre second développeur, tout en continuant à mener à bien [tous les autres projets et actions que nous entreprenons](https://framablog.org/tag/collectivisons-internet-convivialisons-internet/).

Cette année encore, nous avons besoin de vous, de votre soutien, de vos partages, pour nous aider à reprendre du terrain sur le web toxique des GAFAM, et multiplier les espaces de numérique éthique.

Nous avons donc demandé à [David Revoy](https://www.peppercarrot.com/fr/files/framasoft.html) de nous aider à montrer cela sur notre site « [Soutenir Framasoft](https://support.joinpeertube.org) », qu’on vous invite à visiter (parce que c’est beau) et surtout à partager le plus largement possible :

[![Barre de dons Framasoft le 12 décembre 2023, à 30 % - 61341 €](https://framablog.org/wp-content/uploads/2023/12/2023-12-12-Soutenir-Framasoft-1024x545.png)](https://support.joinpeertube.org)

**Si nous voulons boucler notre budget pour 2024, il nous reste trois semaines pour récolter 138 659 € : nous n’y arriverons pas sans votre aide !**

<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" rel="nofollow noreferrer noopener" href="https://support.joinpeertube.org/">
      Soutenir Framasoft
   </a>
</div>

