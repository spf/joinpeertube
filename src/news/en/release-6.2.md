---
id: release-6.2
title: "PeerTube 6.2 is out!"
date: July 16, 2024
---

This new version is all about making your life easier. Easier moderation, easier subtitle creation and easier video highlighting. Let's take a look!

#### Comments moderation

Moderation is always the least exciting part and can also be the most time-consuming. To help you manage this, PeerTube v6.2 not only adds a new page listing all the comments made on your videos, which you can now approve manually, but also the ability to create word lists that will automatically put comments containing them in the moderation queue!

For example, if you want to prevent negative comments from appearing directly on your videos, you can create a word list:

![image autotag policies](/img/news/release-6.2/en/moderation_autotag_policy.png)

This means that comments containing them will automatically be put on pending review for approval:

![image of a pending comment](/img/news/release-6.2/en/moderation_comment_pending.png)

You can then approve or reject them on the page where the comments are listed:

![image comments list](/img/news/release-6.2/en/moderation_comments_videos_list.png)

This feature is also available for administrators! They can easily display videos uploaded to their instance that contain specific keywords. This can be useful for monitoring content that doesn't comply with your terms of service, or for monitoring a sporadic phenomenon that is particularly viral.

The idea for this feature came from the study conducted by [the German ISD](https://joinpeertube.org/news/isd-study). Our goal is to improve this in the future, when we envision users being able to subscribe to and share lists of words to make moderation of accounts and instances easier!

#### Automatic transcription

In version 5.2 of PeerTube, we introduced the concept of [*PeerTube runner*](https://docs.joinpeertube.org/admin/remote-runners), a small program that allows PeerTube to perform heavy tasks (such as video encoding) remotely.

[France Université Numérique](https://www.fun-mooc.fr), a public interest group running a MOOC (Massive Open Online Course) platform with a lot of videos, was interested in the flexible architecture of the PeerTube runner for their own use.

In order to improve the accessibility of videos and at the same time contribute to the PeerTube ecosystem, France Université Numérique and the [NLnet Foundation](https://nlnet.nl/project/PeerTube-mobile/), through the [NGI Zero Entrust fund](https://nlnet.nl/entrust/), funded the development of automatic video transcription within PeerTube and the *PeerTube runners*. This transcription makes it possible to automatically create subtitles for videos using an artificial intelligence tool, [Whisper](https://en.wikipedia.org/wiki/Whisper_(speech_recognition_system)), which provides very good results. The artificial intelligence engine and its model can be customised in the configuration.

This new feature can be enabled by PeerTube admins to automatically generate subtitles for newly published videos. The creation of subtitles can also be triggered on demand, allowing administrators to create subtitles for videos published before PeerTube 6.2. This automatic subtitling can also be done by *PeerTube runners*, as it requires a lot of computing power. France Université Numérique is already planning to use this method to subtitle all its content, which represents 3,500 hours of video.

Many thanks to NLnet ([whose funding, unfortunately, is in danger of drying up](https://www.ow2.org/view/Events/The_European_Union_must_keep_funding_free_software_open_letter)) and France Université Numérique for their help and contribution!

#### And more…

As well as making your moderation life easier, PeerTube offers a new way of creating a thumbnail preview for your videos (in addition to the ability to select it from your device): directly from an image in your video! Play the video down to the image that perfectly illustrates the content and display it. Thanks to [Kent Anderson](https://github.com/Chocobozzz/PeerTube/pull/6424) for this external contribution!

To make sure you don't miss a live broadcasts, they are now highlighted on the *Recently Added*, *Trending*, *Account videos* and *Channel videos* pages.

And as with every new version, you can find all the bug fixes, improvements and other changes in [the changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.0)!

---

You want to help us improve PeerTube? You can do so by sharing this information, by [suggesting improvements](https://ideas.joinpeertube.org/) and, if you can afford it, by [donating to Framasoft](https://support.joinpeertube.org/), the non-profit that develops PeerTube.

Thanks in advance for your support!
Framasoft
