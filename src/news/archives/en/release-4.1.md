---
id: release-4.1
title: PeerTube v4.1 is out!
date: February 23, 2022
---

Hi everybody,

We invite you to discover the improvements and features of this latest version of PeerTube. Let's look around and see what it brings us...

#### A video player more practical, especially on mobile devices

To make videos watching more enjoyable, especially on mobile devices, we've improved several parts of the video player. Now, when you tap the center of the player, a **button is overlayed and allows you to easily play/pause** without using the control bar.

We have increased the control bar size (located at the bottom of the player): so that you can now access more easily the play/pause button and the sound, display or speed settings.

For those who watch videos from a mobile device, we have set up an automatic landscape display of videos when you select full screen view. And if you double-tap on the right or left side of the video player, it allows you to move forward / rewind by 10 seconds without using the control bar. Really handy to easily navigate through a video!

Did you notice that when you press the `?` key it brings you up the keyboard shortcuts list? This feature has been around for a while, but we haven't highlighted it before. As we have made some fixes, this is a good opportunity to introduce it to you.

![](/img/news/release-4.1/en/EN-Keyboard-shortcuts.jpg)


#### An improved plugin system

The improvements we have made to the plugin system allow developers to create new plugins types:

  * **plugins to create specific pages integrated into PeerTube interface**
Some instances wishing to add other content than the by-default PeerTube pages will now be able to do so without losing the graphical context of PeerTube interface.

  * **plugins to add fields to the video updating form**
It was already possible to add new description fields to a video via a plugin, but these were visible only in a specific tab ("Plugin Settings"). It is now possible to make these new fields appear in the "Basic Information" tab, which makes them more visible.

As a reminder, instance administrators can enable / disable plugins in the `Administration` menu, `Plugins/Themes` tab and you also can discover our [plugins selection](https://joinpeertube.org/plugins-selection).


#### New filters on search results

Now, when you use the search bar of your favorite instance, you can filter the results so that only one type of result is displayed, choosing from `videos`, `channels` or `playlists`. Very useful to find channels or playlists on a specific topic.

![](/img/news/release-4.1/en/EN-Filtre-types-resultat.jpg)

#### More instances customizations

So that PeerTube instances administrators could customize their instance, we now allow them to:

  * **specify default privacy type on new uploaded videos**
Until now, the privacy type was "public" for any video added. Admins can now decide that all videos uploaded on their instance will be in unlisted / private / internal privacy by default (your choice). Users could change this default choice.

  * **specify default licence on new uploaded videos**
Before this version, the license field on uploaded videos was not filled in by default. Admins can now choose a default value for this field and apply it to all new uploaded content. Uploaders will then be able to change this default choice.

  * **disable some features**
As you were so many to ask for it, it is now possible to disable downloading and/or commenting on all your instance videos.

All these changes are to be made in the instance's configuration file.

#### About peer-to-peer broadcasting

PeerTube uses a peer-to-peer (P2P) protocol to broadcast highly viewed videos (viral videos), lowering the load of their hosts. From now, instance administrators can disable this feature by default. Users (logged in or anonymous) can however re-enable it if they wish.

You can also disable peer-to-peer when you want to embed a video on external web pages. To do this, simply uncheck the `P2P` box that appears in the `Embed` tab of the video sharing window.

![](/img/news/release-4.1/en/EN-embed-p2p.jpg)


#### And also

We have added a configurable login behaviour: if one or more external authentication plugins are installed on an instance, specific buttons will appear below the login form. If only one external authentication plugin has been installed, the instance administrators can enable an automatic redirection of users to the external authentication platform when they click on the login button.

We have made many other improvements in this new version. You can read the whole list on https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md.

Thanks to all PeerTube contributors!
Framasoft
