---
id: release-sepiasearch
title: Introducing SepiaSearch!
date: September 22, 2020
---

<p>Hi everybody,</p>

<p>
  We have just released <a target="_blank" href="https://sepiasearch.org">SepiaSearch</a>, our search engine to help you
  discover videos and channels on PeerTube!
</p>

<p>
  We've worked hard to ensure that this engine respects your data, your attention and your freedoms.
</p>

<ul>
  <li>Learn more <a target="_blank"
      href="https://framablog.org/2020/09/22/sepia-search-our-search-engine-to-promote-peertube/">on the Framablog</a>
  </li>
  <li>Use <a target="_blank" href="https://sepiasearch.org">SepiaSearch</a></li>
  <li>Copy and adapt <a target="_blank" href="https://framagit.org/framasoft/peertube/search-index">the
      code</a>, to make your own PeerTube search engine.</li>
</ul>

<a target="_blank" href="https://sepiasearch.org">
  <img loading="lazy" src="/img/news/release-sepiasearch/en/sepiasearchbar.png" alt="">
</a>

<p>
  Please remember to share <a target="_blank" href="https://joinpeertube.org/roadmap/">the roadmap to v3 page</a>, where
  people can learn more about (and support) ou plans for PeerTube.
</p>

<p>
  Thanks to all PeerTube contributors!
  <br />
  Framasoft
</p>
