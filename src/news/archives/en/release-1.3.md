---
id: release-1-3-0
title: PeerTube 1.3 is out!
date: June 5, 2019
twitter: https://twitter.com/joinpeertube/status/1136210369066819584
---

Hello!

We've just released PeerTube 1.3 and it brings a lot of new features.

The most important of these new features is __the playlist system__. This feature allows any user to create a playlist in which it's possible to add videos and reorder them. Videos added to a playlist can be viewed entirely or partially: the creator of the playlist can decide when the video playback starts and/or ends (timecode system). This system is really useful to create all kinds of zappings or educational contents by selecting extracts from videos which interest you. In addition, a "Watch Later" playlist is created by default for each user. Thus, you can save videos in this playlist when you don't have time to watch them immediately.

Another feature of this 1.3 version has been entirely developed by an external contributor: [Josh Morel](https://github.com/joshmorel) who add __a quarantine system__ for videos on PeerTube. If the administrator of an instance enables this feature, any new video uploaded on his instance will automatically be hidden until a moderator approves it.

PeerTube translation community have done a huge job. __3 new languages__ are now available: Japanese, Dutch and European Portuguese (PeerTube already support Brazilian Portuguese). Amazing! PeerTube is now available in 19 languages!

Now, administrators can __manage more finely how other instances subscribe to their own instance__. The administrator can decide whether or not to approve the subscription of another instance to its own. It is also  possible to activate automatic rejection for any new subscription to its instance. Finally, a notification is created as soon as the administrator's instance receives a new subscription. These features help administrators control on which instances their content is displayed.

We're also redesigning the __PeerTube video player__ to offer better video playback and to correct a few bugs. With this new player, resolution changes should be smoother and the bandwidth management is optimized with a more efficient buffering system. Version 1.3 of PeerTube also adds ability for administrators to enable this new experimental player so we can get feedback on it. We hope to use this new player by default in the future.

Finally, we have made some adjustments to the __user interface__ so it easier and nicer to use. For instance, video thumbnails are becoming bigger so that they're more highlighted. Users now have a quick access to their library from the menu that includes their playlists, videos, video watching history and their subscriptions.

Many other improvements have been made in this new version. You can see the complete list on https://github.com/Chocobozzz/PeerTube/releases/tag/v1.3.0.

Thanks to all PeerTube contributors!
Framasoft
