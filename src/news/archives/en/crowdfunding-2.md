---
id: crowdfunding-2
title: 'PeerTube crowdfunding newsletter #2'
date: 'August 20, 2018'
---

Hello everyone!

The development of the crowdfunding features is going well.

As a reminder, in the first newsletter (July 23rd, 2018), we announced that the localization system and RSS feeds were implemented, and that we were making progress on the subtitles support and the advanced search.

These four features are all implemented, and can already be used on instances updated to version _v1.0.0-beta.10_ (for example https://framatube.org). Regarding the subtitles support, you can test them on the [the "What is PeerTube"](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3) video.

We are currently finishing the video import system, from a URL (YouTube, Vimeo etc) or a torrent file. This feature should be available in a few days, when we will release a new version (_v1.0.0-beta.11_).

The import system will complete the first crowdfunding goal. The next feature we will be working on will be the user subscriptions.

We remind you that you can track the progress of the work directly [on the git repository](https://github.com/Chocobozzz/PeerTube), and be part of the discussions/bug reports/feature requests in the "Issues" tab.

Moreover, you can ask questions on [the PeerTube forum](https://framacolibri.org/c/qualite/peertube). You can also contact us directly on https://contact.framasoft.org.

Cheers,
Framasoft
