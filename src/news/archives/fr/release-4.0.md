---
id: release-candidate-4.0
title: Apprenez tout sur PeerTube v4 !
date: November 30, 2021
---

Bonjour,

PeerTube v4 est en route, puisque nous venons de publier une version *release candidate* qui sera testée dans les prochaines semaines.

À cette occasion, nous avons publié un billet de blog résumant une année de travail et d'améliorations sur l'écosystème PeerTube. Il présente également les nouvelles fonctionnalités de PeerTube v4, et ce sur quoi nous pensons pouvoir travailler l'année prochaine.

* Découvrez ces nouvelles fonctionnalités [sur notre blog](https://framablog.org/2021/11/30/peertube-v4-prenez-le-pouvoir-pour-presenter-vos-videos/) ;
* Consultez le [code de la version candidate v4](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md#v400-rc1) par vous-même ;
* Soutenez PeerTube en [soutenant Framasoft](https://support.joinpeertube.org/fr), et découvrez toutes nos actions.

![](/img/news/release-4.0/fr/peertube-live.jpg)

Nous tenons à remercier toutes les personnes qui ont contribué à l'écosystème PeerTube tout au long de l'année : vous êtes tous formidables ! Nos remerciements vont également à NLnet, dont la bourse de 50 000 € a financé environ deux tiers de nos dépenses en 2021 pour le projet PeerTube. Le tiers restant provient directement du budget de notre association, qui est financée par vos dons que nous recevons pour l'ensemble de nos actions.

Si vous souhaitez nous aider à financer notre travail en 2022 sur PeerTube et de nombreux autres projets, pensez à [soutenir Framasoft](https://support.joinpeertube.org/fr). Et n'oubliez pas de partager l'info autour de vous !

Nous espérons que vous apprécierez PeerTube v4,
Framasoft
