<template>
  <accordion-element id="Should I have a big server to run PeerTube?">
    <template #title>
      {{ $gettext('Should I have a big server to run PeerTube?') }}
    </template>

    <div>
      <p>{{ $gettext('Minimum hardware requirements:') }}</p>

      <ul>
        <li>{{ $gettext('1 vCore') }}</li>
        <li>{{ $gettext('1.5 GB of RAM') }}</li>
        <li>{{ $gettext('Enough storage for videos') }}</li>
        <li>{{ $gettext('20Mbit/s upload network speed') }}</li>
        <li>{{ $gettext('Enough bandwidth to broadcast videos') }}</li>
      </ul>

      <p>
        <SafeHTML>
          {{ $gettext('Recommended hardware requirements for a big instance to handle 1,000 concurrent viewers (see our <a target="_blank" rel="noopener noreferrer" href="https://joinpeertube.org/news/stress-test-2023">blog post about our viewers stress test</a>):') }}
        </SafeHTML>
      </p>

      <ul>
        <li>{{ $gettext('4 vCore') }}</li>
        <li>{{ $gettext('4 GB of RAM') }}</li>
        <li>{{ $gettext('Enough storage for videos') }}</li>
        <li>{{ $gettext('1Gbit/s upload network speed') }}</li>
        <li>{{ $gettext('Enough bandwidth to broadcast videos') }}</li>
        <li>
          <SafeHTML>
            {{ $gettext('Read the <a target="_blank" rel="noopener noreferrer" href="https://docs.joinpeertube.org/maintain/configuration#scalability">scalability guide</a> if you plan to have many users or viewers', true) }}
          </SafeHTML>
        </li>
      </ul>

      <p>{{ $gettext('If you plan to do transcoding on the same machine as the PeerTube instance:') }}</p>

      <ul>
        <li>{{ $gettext('8 vCore') }}</li>
        <li>{{ $gettext('8 GB of RAM') }}</li>
      </ul>

      <p>{{ $gettext('For a more detailed guide about hardware requirements:') }}</p>

      <strong>{{ $gettext('CPU') }}</strong>

      <p>
        {{
          $gettext('Except for video transcoding, a PeerTube instance is not CPU bound. Neither Nginx, PeerTube itself, PostgreSQL nor Redis require a lot of computing power. If it were only for those, one could easily get by with just one thread/vCPU.')
        }}
      </p>

      <p>
        {{ $gettext('You will hugely benefit from at least a second thread though, because of transcoding. Transcoding is very cpu intensive.') }}

        {{ $gettext('It serves two purposes on a PeerTube instance: it ensures all videos can be played optimally in the web interface, and it generates different resolutions for the same video.') }}
      </p>

      <p>
        <SafeHTML>
          {{ $gettext('Transcoding can also be offloaded to other machines using <a target="_blank" rel="noopener noreferrer" href="https://docs.joinpeertube.org/admin/remote-runners">remote runners</a>.', true) }}
        </SafeHTML>
      </p>

      <strong>{{ $gettext('RAM') }}</strong>

      <p>
        {{ $gettext('1.5 GB of RAM should be plenty for a basic PeerTube instance, which usually takes at most 500 MB in RAM.') }}

        {{ $gettext('The only reason you might want more would be if you colocate your Redis or PostgreSQL services on a non-SSD system.') }}
      </p>

      <strong>{{ $gettext('Storage') }}</strong>
      <p>
        {{ $gettext('There are two important angles to storage: disk space usage and sustained read speed.') }}

        {{ $gettext('To make a rough estimate of your disk space usage requirements, you want to know the answer to three questions:') }}
      </p>

      <ul>
        <li>{{ $gettext('What is the total size of the videos you wish to stream?') }}</li>
        <li>
          {{ $gettext('Do you want to enable transcoding? If so, do you want to provide multiple resolutions per video? Try this out with a few videos and you will get an idea of how much extra space is required per video and estimate a multiplication factor for future space allocation.') }}
        </li>
        <li>{{ $gettext('Which sharing mechanisms do you want to enable? Just web video, or also HLS with P2P? If you want both, this will double your storage needs.') }}</li>
      </ul>

      <p>
        <SafeHTML>
          {{ $gettext('If you want to store many videos on your PeerTube instance, you may want to store videos externally using <a href="https://docs.joinpeertube.org/maintain/remote-storage" target="_blank">Object Storage</a>.', true) }}
        </SafeHTML>
      </p>

      <p>
        {{ $gettext('In terms of read speed, you want to make sure that you can saturate your network uplink serving PeerTube videos.') }}

        {{ $gettext('This should not be a problem with SSD disks, whereas traditional HDD should be accounted for: typical sustained read rates for a well tuned system with a 7200rpm hard disk should hover around 120 MB/s or 960 Mbit/s. The latter should be enough for a typical 1 Gbit/s network uplink.') }}
      </p>

      <strong>{{ $gettext('Network') }}</strong>
      <p>
        {{ $gettext('A rough estimate of a traditional server\'s video streaming network capacity is usually quite straightforward.') }}
        {{ $gettext('You simply divide your server\'s available bandwidth by the average bandwidth per stream, and you have an upper bound.') }}
      </p>

      <p>
        {{ $gettext('Take a server for example with a 1 Gbit/s uplink for example pushing out 1080p60 streams at 5 Mbit/s per stream.') }}
        {{ $gettext('That means the absolute theoretical upper capacity bound is 200 simultaneous viewers if your server\'s disk i/o can keep up.') }}
        {{ $gettext('Expect a bit less in practice.') }}
      </p>

      <p>
        {{ $gettext('But what if you need to serve more users? That\'s where PeerTube\'s federation feature shines. If other PeerTube instances following yours, chances are they have decided to mirror part of your instance!') }}

        <SafeHTML>
          {{ $gettext('The feature is called <a href="https://docs.joinpeertube.org/admin/following-instances#instances-redundancy" target="_blank">"server redundancy"</a> and caches your most popular videos to help serve additional viewers.') }}
        </SafeHTML>

        {{ $gettext('While viewers themselves contribute a little additional bandwidth while watching the video in their browsers (mostly during surges), mirroring servers have a much greater uplink and will help your instance with sustained higher concurrent streaming.') }}
      </p>
    </div>
  </accordion-element>
</template>

<script>
  import { defineComponent } from 'vue'

  import AccordionElement from '../shared/AccordionElement.vue'

  export default defineComponent({
    components: {
      AccordionElement
    }
  })
</script>
