import { isDefaultLocale, getShortLocale } from '../shared/i18n'

export default {
  methods: {
    buildImgUrl: function (imageName) {
      return import.meta.env.BASE_URL + 'img/' + imageName
    },

    buildPublicUrl: function (name) {
      return import.meta.env.BASE_URL + name
    },

    buildRoute: function (route) {
      if (this.localePath && !isDefaultLocale(this.localePath)) {
        return '/' + this.localePath + route
      }

      return route
    },

    getCurrentShortLocale () {
      return getShortLocale(this.$language.current)
    },

    async lazyRenderMarkdown (markdown) {
      const { renderMarkdown } = await import('../shared/markdown-render')

      return renderMarkdown(markdown)
    }
  }
}
